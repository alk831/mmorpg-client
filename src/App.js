import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import logo from './logo.svg';
import './App.css';

import Home from './pages/home';
import Game from './pages/game';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
        <Link to="/">Strona główna</Link>
        <Link to="/game">Gra</Link>
        <header className="App-header">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
          <Route path="/" exact component={Home}/>
          <Route path="/game" component={Game}/>


          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    </Router>
    );
  }
}

export default App;
