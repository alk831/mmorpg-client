import React from 'react';
import './style.scss';

const BaseInput = ({ type = 'text', id }) => (
  <input
    id={id}
    type={type}
    className="base-input"
  />
);

export default BaseInput;