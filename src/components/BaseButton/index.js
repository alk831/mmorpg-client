import React from 'react';
import './style.scss';

import Text from '../Text';

const BaseButton = ({ title, theme = 'green', children, className = '' }) => (
  <button
    className={`base-button base-button--${theme} ${className}`}
    title={title}
  >
    {
      (title && <Text text={title} className="base-button__text"/>)
      || {children}
    }
  </button>
);

export default BaseButton;