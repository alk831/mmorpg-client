import React from 'react';
import './style.scss';

const Text = ({ text, children, className }) => (
  <span className={"text " + className}>
    {(children && children) || text}
  </span>
);

export default Text;