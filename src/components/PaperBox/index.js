import React from 'react';
import './style.scss';

import Text from '../Text';

const PaperBox = ({ children, title, theme = 'paper', className = '' }) => (
  <div className={`paper-box paper-box--${theme} ${className}`}>
    {title &&
      <p className="paper-box__bar">
        <Text text={title}/>
      </p>
    }
    <div className="paper-box__content">
      {children}
    </div>
  </div>
);

export default PaperBox;