import React from 'react';

import BaseButton from '../../components/BaseButton';
import BaseInput from '../../components/BaseInput';
import Text from '../../components/Text';
import PaperBox from '../../components/PaperBox';

const Home = () => (
  <div>
    <h2>Homepage</h2>
    <BaseButton title="Zaloguj się" upper/>
          <p></p>
    <BaseButton title="Zaloguj się" theme="red"/>
    <PaperBox title="Logowanie" theme="green" className="login__wrapper">
      {/* <label htmlFor="login">Login:</label> */}
      <Text>Login:</Text>
      <BaseInput id="login"/>
      <label htmlFor="password"><Text>Hasło:</Text></label>
      
      <BaseInput type="password" id="password"/>
      <BaseButton
        className="login__button"
        title="ZALOGUJ SIĘ"
        theme="red"
      />
    </PaperBox>
  </div>
);

export default Home;