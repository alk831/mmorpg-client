import React from 'react';

class Game extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      ctx: {},
      cw: 0,
      ch: 0,
      mw: 0,
      mh: 0,
      playerX: 0,
      playerY: 0,
      keys: {},
      lastKey: null
    };

  }

  playerMove() {
    let { keys, lastKey, playerX, playerY } = this.state;
    const playerSpeed = 2;

    if (lastKey === 'd' && keys[68]) playerX += playerSpeed;

    this.setState({ playerX, playerY });
  }

  playerLimits = () => {
    const { cw, ch, mw, mh } = this.state;
    return {
      x: [cw/2, mw - cw/2 - 64],
      y: [ch/2, mh - ch/2]
    }
  }

  renderMap(map) {
    const { cw, playerX } = this.state;
    const { naturalWidth, naturalHeight } = map;
    const { x, y } = this.playerLimits();

    let mapX = 0;
    let mapY = 0;


    if (playerX <= x[0]) mapX = 0;
    else if (playerX >= x[1]) mapX = x[1] - cw / 2;
    else mapX = playerX - cw/2;

    // console.log(playerX)

    console.log(mapX)


    this.state.ctx.drawImage(map, mapX, mapY,
      naturalWidth,
      naturalHeight,
      0, 0,
      naturalWidth,
      naturalHeight
    );
  }

  startEngine() {

    const map = new Image();
    map.src = 'http://aldous.margonem.pl/obrazki/miasta/osada-smialkow.2.png';
    const { naturalWidth: mw, naturalHeight: mh } = map;
    this.setState({ mw, mh });
  
    const render = () => {
      requestAnimationFrame(render);
      this.renderMap(map);
      this.playerMove();
    };

    map.onload = render;
  }

  addListeners() {
    const btnKeys = {		
			87: 'w',
			65: 'a',
			83: 's',
			68: 'd'
		};
    document.body.addEventListener('keydown', e => {
      this.setState(({ keys }) => ({
        keys: { ...keys, [e.keyCode]: true },
        lastKey: btnKeys[e.keyCode]
      }));
    });
    document.body.addEventListener('keyup', e => {
      this.setState(({ keys }) => ({
        keys: { ...keys, [e.keyCode]: false }
      }));
    });
  }

  
  componentDidMount() {
    const { canvas } = this.refs;

    const cw = canvas.width = 512;
    const ch = canvas.height = 512;
    const ctx = canvas.getContext('2d');

    this.setState({ ctx, cw, ch });
    this.addListeners();
    this.startEngine();
  }

  render() {
    return (
      <div>
        <canvas ref="canvas"/>
        {this.state.playerX} - {this.state.playerY}
        {JSON.stringify(this.playerLimits())}
      </div>
    )
  }
}


export default Game;